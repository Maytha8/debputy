from typing import TypedDict, NotRequired, List, Any, Literal, Optional

LintSeverity = Literal["style"]


class DiagnosticData(TypedDict):
    quickfixes: NotRequired[Optional[List[Any]]]
    lint_severity: NotRequired[Optional[LintSeverity]]
